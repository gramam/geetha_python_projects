# RegEx is used to search for string patterns in a string
# Python has built in package called re, which is used to work with reg expressions

# to SEARCH a string that starts with something and ends with something

import re
txt = "Hi! How are you? Tell me about yourself"
output=re.search("^H.*self$",txt) # you can give jsut one or two starting letter and the last few letters of last word to find for a match
#print(output) # Doesn't give any o/p since it only does the operation
if output:
        print("Yes, there is a match found!")
else:
        print("There is no match!")

# "findall" - returns a LIST containing all matches. o/p will be in the form of list.
y=re.findall('you.*f',txt)
print(y)



#Replace all white-space characters with the digit "9":

txt = "The rain in Spain"
x = re.sub("\s", "9", txt)
print(x)
x = txt.upper()
print(x)
print(txt.replace('\s',"7"))
print(txt)
print(x)


